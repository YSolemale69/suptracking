/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.service;

import com.supinfo.suptracking.dao.CarDao;
import com.supinfo.suptracking.entity.Car;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Pascal
 */
@Stateless
public class CarService {
    @EJB
    private CarDao carDao;
     
    public Car addCar(Car car) {
       return carDao.addCar(car);
    }
    
    public Car updateCar(Car car) {
       return carDao.updateCar(car);
    }

    public List<Car> getAllCar() {
        return carDao.getAllCar();
    }
    
    public Car deleteCar(Long carId){
        return carDao.deleteCar(carId);
    }
    
    public List<Car> getCarByUser(Long userId) {
        return carDao.getCarByUser(userId);
    }
    
    public Car findCarById(Long carId){
        return carDao.findCarById(carId);
    }
}
