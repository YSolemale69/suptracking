/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.service;

import com.supinfo.suptracking.dao.UserDao;
import com.supinfo.suptracking.entity.User;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Pascal
 */
@Stateless
public class UserService {

    @EJB
    private UserDao userDao;
     
    public User addUser(User user) {
       return userDao.addUser(user);
    }
    
    public User updateUser(User user) {
       return userDao.updateUser(user);
    }

    public List<User> getAllUser() {
        return userDao.getAllUser();
    }

    
    public User findUserByUsernameAndPassword(String username, String Password){
        return userDao.findUserByUsernameAndPassword(username, Password);
    }
    
    public User findUserById(Long UserId){
        return userDao.findUserById(UserId);
    }
}
