/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.service;

import com.supinfo.suptracking.dao.ZoneDao;
import com.supinfo.suptracking.entity.Zone;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Pascal
 */
@Stateless
public class ZoneService {
    @EJB
    private ZoneDao zoneDao;
     
    public Zone addZone(Zone zone) {
       return zoneDao.addZone(zone);
    }

    public List<Zone> getAllZone() {
        return zoneDao.getAllZone();
    }
    
    public Zone deleteZone(Long zoneId){
        return zoneDao.deleteZone(zoneId);
    }
    
    public List<Zone> getZoneByCar(Long zoneId) {
        return zoneDao.getZoneByCar(zoneId);
    }
}
