/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.beans;

import com.supinfo.suptracking.entity.Customer;
import com.supinfo.suptracking.dao.CustomerFacade;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import java.util.List;

/**
 *
 * @author Opium
 */
@ManagedBean
@RequestScoped
public class CustomerBean {
    
    @EJB
    private CustomerFacade customerFacade;
    
    private static final String CREATE_CUSTOMER = "createCustomer";
    private static final String DELETE_CUSTOMER = "deleteCustomer";   
    private static final String UPDATE_CUSTOMER = "updateCustomer";
    private static final String LIST_ALL_CUSTOMERS = "listAllCustomers";
    private static final String STAY_IN_THE_SAME_PAGE = null;

    private Customer customer;

    public Customer getCustomer() {
		
	if(customer == null){
            customer = new Customer();
	}
		
        return customer;
    }

    public void setCustomer(Customer customer) {
	this.customer = customer;
    }

    public List<Customer> getAllCustomers() {
	return customerFacade.findAll();
    }

    public String updateCustomerStart(){
	return UPDATE_CUSTOMER;
    }
	
    public String updateCustomerEnd(){
	try {
            customerFacade.update(customer);
	} catch (EJBException e) {
            sendErrorMessageToUser("Error. Check if the weight is above 0 or call the adm");
            return STAY_IN_THE_SAME_PAGE;
	}
		
            sendInfoMessageToUser("Operation Complete: Update");
            return LIST_ALL_CUSTOMERS;
    }
	
    public String deleteCustomerStart(){
	return DELETE_CUSTOMER;
    }
	
    public String deleteCustomerEnd(){
	try {
            customerFacade.delete(customer);
	} catch (EJBException e) {
            sendErrorMessageToUser("Error. Call the ADM");
            return STAY_IN_THE_SAME_PAGE;
	}			
		
    sendInfoMessageToUser("Operation Complete: Delete");
		
    return LIST_ALL_CUSTOMERS;
    }
	
    public String createCustomerStart(){
	return CREATE_CUSTOMER;
    }
	
    public String createCustomerEnd(){
	try {
            customerFacade.save(customer);
        } catch (EJBException e) {
            sendErrorMessageToUser("Error. Check if the weight is above 0 or call the adm");
			
            return STAY_IN_THE_SAME_PAGE;
	}		
		
        sendInfoMessageToUser("Operation Complete: Create");
    
        return LIST_ALL_CUSTOMERS;
    }
	
    public String listAllCustomers(){
	return LIST_ALL_CUSTOMERS;
    }
	
    private void sendInfoMessageToUser(String message){
	FacesContext context = getContext();
	context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, message));
    }
	
    private void sendErrorMessageToUser(String message){
	FacesContext context = getContext();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, message));
    }
	
    private FacesContext getContext() {
	FacesContext context = FacesContext.getCurrentInstance();
	return context;
    }
}