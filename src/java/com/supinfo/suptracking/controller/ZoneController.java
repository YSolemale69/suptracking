/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.controller;

import com.supinfo.suptracking.entity.Car;
import com.supinfo.suptracking.entity.Zone;
import com.supinfo.suptracking.service.CarService;
import com.supinfo.suptracking.service.ZoneService;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Pascal
 */
@ManagedBean
@RequestScoped
public class ZoneController {

        @EJB
        ZoneService zoneService;
        CarService carService;
        
        Car car = new Car();
        Zone zone = new Zone();
        
        public String add(Long carId){
            car = carService.findCarById(carId);
            zone.setCar(car);
            zoneService.addZone(zone);
            return "cars?faces-redirect=true";
        }

        public String delete(Long zoneId){
            zoneService.deleteZone(zoneId);
            return "cars?faces-redirect=true";
        }
        public Zone getZone() {
            return zone;
        }

        public void setZone(Zone zone) {
            this.zone = zone;
        }     
        
        public DataModel<Zone> getZoneDatamodel(Long carId) {
        List<Zone> zones = zoneService.getZoneByCar(carId);
        DataModel<Zone> zoneDataModel = new ListDataModel<>(zones);
        return zoneDataModel;
    }
}
