/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.controller;

import com.supinfo.suptracking.entity.User;
import com.supinfo.suptracking.service.UserService;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Pascal
 */
@ManagedBean
@SessionScoped
public class UserController {

    @EJB
    UserService userService;

    private User user = new User();
    private String username;
    private String password;
    
    public User getUser() {
        return user;
    }

    public String login() {
        String forward = "login";
        
        user = userService.findUserByUsernameAndPassword(username, password);
        if (user!=null) {
            //HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            //request.getSession(false).setAttribute("user", user);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
            forward = "cars?faces-redirect=true";
        }
        return forward;
    }

    public String register(){
        userService.addUser(user);
        //HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        //request.getSession(false).setAttribute("user", user);
        FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("user", user);
        return "cars?faces-redirect=true";
    }
    
    public String logout() {
        user = null;
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "login?faces-redirect=true";
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
