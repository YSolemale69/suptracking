/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.controller;

import com.supinfo.suptracking.entity.Car;
import com.supinfo.suptracking.entity.User;
import com.supinfo.suptracking.service.CarService;
import java.sql.Timestamp;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.DataModel;
import javax.faces.model.ListDataModel;

/**
 *
 * @author Pascal
 */
@ManagedBean
@RequestScoped
public class CarController {

    @EJB
    CarService carService;
    
    User user = (User) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("user");
    
    private Car car = new Car();
    
    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
    
    public String add(){
        car.setLatitude(10.10);
        car.setLongitude(12.12);
        car.setUpdateTimestamp(new Timestamp(System.currentTimeMillis()));
        car.setUser(user);
        carService.addCar(car);
        return "cars?faces-redirect=true";
    }
    
    public String edit(Long carId){
        Car oldcar = carService.findCarById(carId);
        oldcar.setName(car.getName());
        oldcar.setBrand(car.getBrand());
        oldcar.setEntryYear(car.getEntryYear());
        oldcar.setUpdateTimestamp(new Timestamp(System.currentTimeMillis()));
        carService.updateCar(oldcar);
        return "cars?faces-redirect=true";
    }
    public String delete(Long carId){
        carService.deleteCar(carId);
        return "cars?faces-redirect=true";
    }
    
    public DataModel<Car> getCarDatamodel() {
        List<Car> cars = carService.getCarByUser(user.getId());
        DataModel<Car> carDataModel = new ListDataModel<>(cars);
        return carDataModel;
    }
}
