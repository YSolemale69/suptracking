/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author Opium
 */
@Entity
@Table(name = "users")
@NamedQuery(name="Users.findUserByEmail", query="select u from Users u where u.email = :email")
public class Users {
    public static final String FIND_BY_EMAIL = "Users.findUserByEmail";
   
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(unique = true)
    private String email;
    private String password;
    private String name;
    private String role;
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
		return email;
	}

    public void setEmail(String email) {
	this.email = email;
    }

    public String getPassword() {
	return password;
    }

    public void setPassword(String password) {
	this.password = password;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	
    public String getRole() {
	return role;
    }

    public void setRole(String role) {
    	this.role = role;
    }

    @Override
    public int hashCode() {
	return getId();
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if(object instanceof Users){
            Users user = (Users) object;
            return user.getEmail().equals(getEmail());
	}
		
        return false;
    }
    
}
