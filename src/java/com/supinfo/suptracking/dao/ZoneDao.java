/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import com.supinfo.suptracking.entity.Zone;
import java.util.List;

/**
 *
 * @author Pascal
 */
public interface ZoneDao {
    public Zone addZone(Zone zone);

    public List<Zone> getAllZone();
    
    public Zone deleteZone(Long zoneId);
    
    public List<Zone> getZoneByCar(Long zoneId);
}