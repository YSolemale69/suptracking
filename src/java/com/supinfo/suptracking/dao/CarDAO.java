/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import com.supinfo.suptracking.entity.Cars;

import javax.ejb.Stateless;

/**
 *
 * @author Opium
 */
@Stateless
public class CarDAO extends GenericDAO<Cars>{

    public CarDAO(){
        super(Cars.class);
    }
}
