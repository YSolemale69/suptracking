/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import java.util.HashMap;
import java.util.Map;

import com.supinfo.suptracking.entity.Users;

import javax.ejb.Stateless;

/**
 *
 * @author Opium
 */
@Stateless
public class UsersDAO extends GenericDAO<Users>{
    
    public UsersDAO(){
        super(Users.class);
    }
    
    public Users findUserByEmail(String email) {
        Map<String, Object> parameters = new HashMap<String, Object>();
	parameters.put("email", email);

	return super.findOneResult(Users.FIND_BY_EMAIL, parameters);
    }
}
