/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import com.supinfo.suptracking.entity.Car;
import java.util.List;


/**
 *
 * @author Opium
 */
public interface CarDao{
    public Car addCar(Car car);
    
    public Car updateCar(Car car);

    public List<Car> getAllCar();
    
    public Car deleteCar(Long carId);
    
    public List<Car> getCarByUser(Long userId);
    
    public Car findCarById(Long carId);
}