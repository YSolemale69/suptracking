/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao.jpa;

import com.supinfo.suptracking.dao.CarDao;
import com.supinfo.suptracking.entity.Car;
import com.supinfo.suptracking.entity.Car_;
import com.supinfo.suptracking.entity.User_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Pascal
 */

@Stateless
public class JpaCarDao implements CarDao {

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public Car addCar(Car car) {
        em.persist(car);
        return car;
    }

    @Override
    public Car updateCar(Car car) {
        em.merge(car);
        return car;
    }

    @Override
    public List<Car> getAllCar() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Car> query = criteriaBuilder.createQuery(Car.class);
        query.from(Car.class);
        return em.createQuery(query).getResultList();
    }

    @Override
    public Car deleteCar(Long carId) {
        Car car = em.find(Car.class, carId);
        em.remove(car);
        return car;
    }

    @Override
    public List<Car> getCarByUser(Long userId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Car> query = criteriaBuilder.createQuery(Car.class);
        Root<Car> contact = query.from(Car.class);
        query.where(criteriaBuilder.equal(contact.get(Car_.user).get(User_.id), userId));
        
        return em.createQuery(query).getResultList();
    }
    
    @Override
    public Car findCarById(Long carId) {
        return em.find(Car.class, carId);
    }
}

