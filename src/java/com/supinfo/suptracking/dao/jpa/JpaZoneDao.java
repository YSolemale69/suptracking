/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao.jpa;

import com.supinfo.suptracking.dao.ZoneDao;
import com.supinfo.suptracking.entity.Car_;
import com.supinfo.suptracking.entity.Zone;
import com.supinfo.suptracking.entity.Zone_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author Pascal
 */

@Stateless
public class JpaZoneDao implements ZoneDao{

    @PersistenceContext
    private EntityManager em;

    @Override
    public Zone addZone(Zone zone) {
        em.persist(zone);
        return zone;
    }

    @Override
    public List<Zone> getAllZone() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Zone> query = criteriaBuilder.createQuery(Zone.class);
        query.from(Zone.class);
        return em.createQuery(query).getResultList();
    }

    @Override
    public Zone deleteZone(Long zoneId) {
        Zone zone = em.find(Zone.class, zoneId);
        em.remove(zone);
        return zone;
    }

    @Override
    public List<Zone> getZoneByCar(Long carId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<Zone> query = criteriaBuilder.createQuery(Zone.class);
        Root<Zone> contact = query.from(Zone.class);
        query.where(criteriaBuilder.equal(contact.get(Zone_.car).get(Car_.id), carId));
        
        return em.createQuery(query).getResultList();
    }
    
}
