/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao.jpa;

import com.supinfo.suptracking.dao.CustomerDAO;
import com.supinfo.suptracking.entity.Customer;
import com.supinfo.suptracking.dao.CustomerFacade;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import java.util.List;

/**
 *
 * @author Opium
 */
@Stateless
public class CustomerFacadeImp implements CustomerFacade{
    
    @EJB
    private CustomerDAO customerDAO;
    
    @Override
    public void save(Customer customer) {
        isCustomerWithAllData(customer);
		
	customerDAO.save(customer);
    }

    @Override
    public Customer update(Customer customer) {
	isCustomerWithAllData(customer);
		
	return customerDAO.update(customer);
    }
	
    @Override
    public void delete(Customer customer) {
	customerDAO.delete(customer);
    }

    @Override
    public Customer find(int entityID) {
        return customerDAO.find(entityID);
    }

    @Override
    public List<Customer> findAll() {
	return customerDAO.findAll();
    }
	
    private void isCustomerWithAllData(Customer customer){
	boolean hasError = false;
		
        if(customer == null){
            hasError = true;
        }
		
        if (customer.getName() == null || "".equals(customer.getName().trim())){
            hasError = true;
        }
	
        if (hasError){
            throw new IllegalArgumentException("The customer is missing data. Check the name, they should have value.");
        }
    }
}