/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao.jpa;

import com.supinfo.suptracking.dao.UsersDAO;
import com.supinfo.suptracking.entity.Users;
import com.supinfo.suptracking.dao.UsersFacade;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Opium
 */
@Stateless
public class UsersFacadeImp implements UsersFacade {

	@EJB 
	private UsersDAO usersDAO;
	
	public Users findUserByEmail(String email) {
		return usersDAO.findUserByEmail(email);
	}
}
