/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao.jpa;

import com.supinfo.suptracking.dao.UserDao;
import com.supinfo.suptracking.entity.User;
import com.supinfo.suptracking.entity.User_;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Pascal
 */
@Stateless
public class JpaUserDao implements UserDao{

    
    @PersistenceContext
    private EntityManager em;

    @Override
    public User addUser(User user) {
       em.persist(user);
        return user;
    }
    
    @Override
    public User updateUser(User user) {
        em.merge(user);
         return user;
    }
    
    @Override
    public List<User> getAllUser() {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        query.from(User.class);
        return em.createQuery(query).getResultList();
    }
    
    @Override
    public User findUserByUsernameAndPassword(String username, String password) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();

        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> user = query.from(User.class);
        Predicate usernamePred = criteriaBuilder.equal(user.get(User_.username), username);
        Predicate passPred = criteriaBuilder.equal(user.get(User_.password), password);
        query.where(usernamePred, passPred);

        User result;
        try{
            result = em.createQuery(query).getSingleResult();
        }catch(NoResultException e){
            return null;
        }
        return result;
    }
    
    @Override
    public User findUserById(Long userId) {
        return em.find(User.class, userId);
    }
}
