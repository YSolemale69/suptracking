/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import com.supinfo.suptracking.entity.Customer;
import javax.ejb.Local;

import java.util.List;
/**
 *
 * @author Opium
 */
@Local
public interface CustomerFacade {
    
    public abstract void save(Customer customer);

    public abstract Customer update(Customer customer);
	
    public abstract void delete(Customer customer);

    public abstract Customer find(int entityID);

    public abstract List<Customer> findAll();
}
