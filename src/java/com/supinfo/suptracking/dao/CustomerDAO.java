/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import com.supinfo.suptracking.entity.Customer;
import javax.ejb.Stateless;

/**
 *
 * @author Opium
 */
@Stateless
public class CustomerDAO extends GenericDAO<Customer>{

    public CustomerDAO() {
        super(Customer.class);
    }
    
    public void delete(Customer customer) {
        super.delete(customer.getId(), Customer.class);
    }
}
