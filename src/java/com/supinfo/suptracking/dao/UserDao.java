/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import com.supinfo.suptracking.entity.User;
import java.util.List;

/**
 *
 * @author Pascal
 */
public interface UserDao {
    User addUser(User user);
    
    User updateUser(User user);
    
    List<User> getAllUser();
    
    public User findUserByUsernameAndPassword(String username, String password);
            
    public User findUserById(Long UserId);
}
