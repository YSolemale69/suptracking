/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.suptracking.dao;

import com.supinfo.suptracking.entity.Users;
import javax.ejb.Local;

/**
 *
 * @author Opium
 */
@Local
public interface UsersFacade {
    public Users findUserByEmail(String email);
}
